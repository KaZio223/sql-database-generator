"""
Python SQL database generator.
"""

import logging
from database_generator import *

logging.basicConfig(level=logging.INFO, format='%(levelname)s:%(filename)s:%(asctime)s: %(message)s')

number_of_users = 2000
number_of_products = 700
number_of_orders = 15000
database = "MyShop"

if __name__=="__main__":
    with MySQL_db_generator(number_of_users, number_of_products, number_of_products, database) as mdg:
        mdg.initiate_database()
        mdg.generate_data()

