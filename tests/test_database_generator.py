"""
Basic test for MySQL_db_generator class.
"""

from shared_modules import *
from database_generator import *

class test_MySQL_db_generator(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.number_of_users = 10
        cls.number_of_products = 30
        cls.number_of_orders = 50
        cls.database = "test123"


    def setUp(self):
        self.mdg = MySQL_db_generator(self.__class__.number_of_users, self.__class__.number_of_products, self.__class__.number_of_orders, self.__class__.database)
        self.mdg.connect_to_server()


    def tearDown(self):
        if not self.mdg.db.is_connected():
            self.mdg.connect_to_server()
        cursor = self.mdg.db.cursor()
        cursor.execute("SHOW DATABASES")

        if self.__class__.database in [i[0] for i in cursor]:
            cursor = self.mdg.db.cursor()
            cursor = cursor.execute(f"DROP DATABASE {self.__class__.database}")
        
        self.mdg.db.close()
        logging.warning("Connection closed.")
        

    def test_config(self):
        LOGIN = server_config()
        config = LOGIN.return_config()
        self.assertIsInstance(config, dict)
        self.assertIn("host", config.keys())
        self.assertIn("user", config.keys())
        self.assertIn("password", config.keys())


    def test_connect_to_server(self):
        self.assertTrue(self.mdg.db.is_connected())


    def test_run_command(self):
        cursor = self.mdg.run_command("SHOW DATABASES")
        self.assertIsInstance(cursor, mysql.connector.cursor.MySQLCursor)

        for i in cursor:
            db_name = i[0]
            self.assertIsInstance(db_name, str)
            self.assertTrue(len(db_name)>0)
        
        self.assertFalse([i[0] for i in cursor]) # cursor should now be empty after yield(ing)


    def test_connect_to_database(self):
        cursor = self.mdg.run_command("SHOW DATABASES")
        self.assertNotIn(self.__class__.database, [i[0] for i in cursor])

        self.mdg.connect_to_database()
        cursor = self.mdg.run_command("SHOW DATABASES")
        self.assertIn(self.__class__.database, [i[0] for i in cursor])


    def test_prepare_query_from_dict(self):
        expected = {"Users": "`First name` VARCHAR(40), `Last name` VARCHAR(40), `Address` VARCHAR(40), `City` VARCHAR(40), `Post code` VARCHAR(40), `State` VARCHAR(40), `e-mail` VARCHAR(40)",
                    "Products": "`Name` VARCHAR(40), `Model` VARCHAR(40), `Unit price in $` FLOAT(5), `Available` BOOL",
                    "Orders": "`User ID` INT, `Product ID` INT, `Quantity` INT, `Date` DATE"}

        for key, value in expected.items():
            setup = tables_setup.get(self.mdg.tables[key])
            rv = self.mdg.prepare_query_from_dict(setup)
            self.assertEqual(rv, value)


    def test_create_tables(self):
        self.mdg.connect_to_database()
        cursor = self.mdg.run_command("SHOW TABLES")
        self.assertEqual([], [i for i in cursor])

        self.mdg.create_tables()
        cursor = self.mdg.run_command("SHOW TABLES")
        self.assertCountEqual(list(self.mdg.tables.keys()), [i[0] for i in cursor])


    def test_generate_data_for_table(self):
        self.mdg.connect_to_database()
        self.assertTrue(self.mdg.db.is_connected())

        self.mdg.create_tables()
        for n_rows, table_name in zip([self.number_of_users, self.number_of_products, self.number_of_products], 
                                      list(self.mdg.tables.keys())):
            self.mdg.generate_data_for_table(table_name, n_rows)
            cursor = self.mdg.db.cursor()
            cursor.execute(f"SELECT * FROM {table_name}")
            rv = cursor.fetchall()
            self.assertGreaterEqual(len(rv), n_rows)


if __name__=="__main__":
    unittest.main()

