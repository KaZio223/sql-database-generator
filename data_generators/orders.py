"""
Orders generator.
"""

import numpy as np
import random
import datetime
from typing import Union #Python 3.5 - 3.9

class order:
    start_date = datetime.date(2020, 1, 1)
    active_days = (datetime.date(2022, 1, 1) - start_date).days
    data = {"User ID": int,
            "Date": datetime.date}


    def __init__(self, number_of_users: int) -> None:
        self.number_of_users = number_of_users + 1

        self.generate_order()


    def generate_order(self) -> None:
        user_ID = self.choose_user_ID()
        date = self.generate_date()

        self.data = {"User ID": user_ID,
                    "Date": date}


    def choose_user_ID(self) -> int:
        return np.random.randint(1,self.number_of_users)


    def generate_date(self) -> str:
        date = self.start_date + datetime.timedelta(days=random.randrange(self.active_days))
        return str(date)


    def values(self) -> str:
        return str(self.data.values())[13:-2]


    def __getitem__(self, arg: str) -> Union[int, datetime.date]:
        if arg in self.data.keys():
            return self.data[arg]
        else:
            raise ValueError(f"{__class__} does not contain key: '{arg}'. Available keys: {str(self.data.keys())[11:-1]}")


    def __repr__(self) -> str:
        return str(self.data)

