"""
Orders details generator.
"""

import numpy as np
if __name__=="data_generators.ordered_products":
    from data_generators.decorators import validate_int_and_float
else:
    from decorators import validate_int_and_float

class ordered_products:
    EX_products = 4
    std_products = 2
    beta_quantity = 1/1.5 # beat = 1/lambda (exponential distribution)
    data = {"Order ID": int,
            "Product ID": int,
            "Quantity": int}


    def __init__(self, number_of_products: int, order_ID: int) -> None:
        self.number_of_products = number_of_products + 1
        self.order_ID = order_ID
        self.used_products = list()
        self.data = list()

        self.generate_order_details()


    def generate_order_details(self) -> None:
        for _ in range(self.generate_number_of_ordered_products()):
            try:
                inner_data = {"Order ID": self.order_ID,
                            "Product ID": self.choose_product_ID(),
                            "Quantity": self.generate_quantity()}
                self.data.append(inner_data)
            except StopIteration:
                pass


    def choose_product_ID(self) -> int:
        counter = 0
        product = np.random.randint(1,self.number_of_products)
        while product in self.used_products:
            counter += 1
            if counter > self.number_of_products:
                raise StopIteration
            else:
                product = np.random.randint(1,self.number_of_products)
        self.used_products.append(product)
        return product


    @validate_int_and_float
    def generate_number_of_ordered_products(self) -> int:
        return max(int(np.round(np.random.normal(self.EX_products, self.std_products))), 1)

    
    @validate_int_and_float
    def generate_quantity(self) -> int:
        return max(int(np.round(np.random.exponential(self.beta_quantity))), 1)


    def values(self, index: int) -> str:
        return str(self.data[index].values())[13:-2]


    def __len__(self) -> int:
        return len(self.data)


    def __getitem__(self, index: int) -> dict:
        return self.data[index]


    def __repr__(self) -> str:
        return str(self.data)

