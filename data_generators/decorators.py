"""
Set of decorator functions.
"""

from tables_setup import tables_setup


def validate_varchar(func):
    def wrapper(*args):
        rv = func(*args)
        if len(rv) >= tables_setup.max_str_len:
            rv = rv[:tables_setup.max_str_len]
        return rv
    return wrapper


def validate_int_and_float(func):
    def wrapper(*args):
        rv = func(*args)
        type_ = type(rv)
        if len(str(rv)) >= tables_setup.max_float_len:
            rv = str(rv)[:tables_setup.max_float_len]
            rv = type_(rv)
        return rv
    return wrapper
