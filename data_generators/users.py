"""
Users generator.
"""

import random
import names
from random_address import real_random_address
if __name__=="data_generators.users":
    from data_generators.decorators import validate_varchar
else:
    from decorators import validate_varchar

class user:
    domains = ["google.com", "yahoo.com", "hotmail.com", "home.com", "wp.pl", "onet.pl", "icloud.com"]
    data = {"First name": str,
            "Last name": str,
            "Address": str,
            "City": str,
            "Post code": str,
            "State": str,
            "e-mail": str}


    def __init__(self) -> None:
        self.generate_user()
        

    def generate_user(self) -> None:
        first_name = names.get_first_name()
        last_name = names.get_last_name()
        address = real_random_address()

        self.data = {"First name": first_name,
                    "Last name": last_name,
                    "Address": self.get_address(address),
                    "City": self.get_city(address),
                    "Post code": self.get_postal_code(address),
                    "State": self.get_state(address),
                    "e-mail": self.generate_email(first_name, last_name)}


    @validate_varchar
    def get_state(self, address: list, key='state') -> str:
        try:
            return address[key]
        except:
            return ""
    

    @validate_varchar
    def get_postal_code(self, address: list, key='postalCode') -> str:
        return address[key]


    @validate_varchar
    def get_address(self, address: list, key='address1') -> str:
        return address[key]


    @validate_varchar
    def get_city(self, address: list, key='city') -> str:
        try:
            return address[key]
        except:
            return ""


    def choose_domain(self) -> str:
        return random.choice(self.domains)


    @validate_varchar
    def generate_email(self, first_name: str, last_name: str) -> str:
        return f"{first_name}.{last_name}@{self.choose_domain()}"


    def values(self) -> str:
        return str(self.data.values())[13:-2]


    def __getitem__(self, arg: str) -> str:
        if arg in self.data.keys():
            return self.data[arg]
        else:
            raise ValueError(f"{__class__} does not contain key: '{arg}'. Available keys: {str(self.data.keys())[11:-1]}")


    def __repr__(self) -> str:
        return str(self.data)

