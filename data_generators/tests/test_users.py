"""
Test for user class.
"""

from shared_modules import *
from users import *

class test_user(unittest.TestCase):
    
    def setUp(self):
        self.tested_user = user()


    def test_generate_email(self):
        self.assertIsInstance(names.get_first_name(), str)
        self.assertIsInstance(names.get_last_name(), str)

        first_name = "Carol"
        last_name = "Ziolkowsky"
        self.tested_user.domains = ["gmail.com"]

        self.assertEqual(self.tested_user.generate_email(first_name, last_name), "Carol.Ziolkowsky@gmail.com")


    def test_address(self):
        must_be = ['state', 'postalCode', 'address1', 'city']
        address = real_random_address()
        for key in must_be:
            self.assertIn(key, address.keys())

        
    def test_get_state_postal_code_address_city(self):
        address = real_random_address()
        for func in [self.tested_user.get_state, self.tested_user.get_postal_code, self.tested_user.get_address, self.tested_user.get_city]:
            self.assertIsNotNone(func(address))
            self.assertIsInstance(func(address), str)
            self.assertTrue(len(func(address)) >= 2)


    def test_choose_domain(self):
        domains = ["google.com", "yahoo.com", "hotmail.com", "home.com", "wp.pl", "onet.pl", "icloud.com"]
        self.assertIn(self.tested_user.choose_domain(), domains)



if __name__=="__main__":
    unittest.main()

    