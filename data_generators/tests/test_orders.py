"""
Test for order class.
"""

from shared_modules import *
from orders import *

class test_order(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.number_of_users = 1000


    def setUp(self):
        self.tested_order = order(self.number_of_users)


    def test_choose_user_ID(self):
        self.assertIsInstance(self.tested_order.choose_user_ID(), int)
        for i in range(2,1000):
            self.tested_order.number_of_users = i
            self.assertGreaterEqual(self.tested_order.choose_user_ID(), 1)
            self.assertLessEqual(self.tested_order.choose_user_ID(), self.number_of_users)

    def test_generate_date(self):
        rv = self.tested_order.generate_date()
        self.assertIsInstance(rv, str)
        self.assertEqual(len(rv), 10)



if __name__=="__main__":
    unittest.main()

    