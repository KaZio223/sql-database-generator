"""
Test for product class.
"""

from shared_modules import *
from products import *

class test_product(unittest.TestCase):

    def setUp(self):
        self.tested_product = product()


    def test_generate_name(self):
        product = "Smartphone"
        manufacturer = "Apple"
        self.assertEqual(self.tested_product.generate_name(product, manufacturer), "Smartphone Apple")

    
    def test_generate_model(self):
        product = "Monitor"
        manufacturer = "Samsung"
        self.assertIn("SM-", self.tested_product.generate_model(product, manufacturer))
        self.assertTrue(len(self.tested_product.generate_model(product, manufacturer)) == 13)


    def test_generate_unit_price(self):
        self.assertGreaterEqual(self.tested_product.generate_unit_price(), 400)
        self.assertLessEqual(self.tested_product.generate_unit_price(), 2000)


    def test_generate_status(self):
        self.assertIsInstance(self.tested_product.generate_status(), bool)

        True_counter = 0
        n = 10000
        for _ in range(n):
            if self.tested_product.generate_status():
                True_counter += 1
        self.assertGreaterEqual(True_counter/n, self.tested_product.probability_True_False[0]-0.05)
        self.assertLessEqual(True_counter/n, self.tested_product.probability_True_False[0]+0.05)




if __name__=="__main__":
    unittest.main()

