"""
Test for ordered products class.
"""

from shared_modules import *
from orders import *

class test_order(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.number_of_products = 200
        cls.order_ID = 1


    def setUp(self):
        self.tested_order = order(self.number_of_products)


    def test_choose_product_ID(self):
        self.tested_order.number_of_products = 11

        self.tested_order.used_products = range(1,11)
        for _ in range(100):
            with self.assertRaises(StopIteration):
                self.tested_order.choose_product_ID()

        self.tested_order.used_products = list()
        for _ in range(100):
            try:
                self.tested_order.choose_product_ID()
            except StopIteration:
                pass
        self.assertEqual(list(np.sort(self.tested_order.used_products)), list(np.sort([i for i in range(1,11)])))


    def test_random_products(self):
        for func in [self.tested_order.generate_number_of_ordered_products, self.tested_order.generate_quantity]:
            self.assertIsInstance(func(), int)
            self.assertGreaterEqual(func(), 1)



if __name__=="__main__":
    unittest.main()

    