"""
Products generator.
"""

import random
import numpy as np
if __name__=="data_generators.products":
    from data_generators.decorators import validate_varchar, validate_int_and_float
else:
    from decorators import validate_varchar, validate_int_and_float
from typing import Union #Python 3.5 - 3.9

class product:
    characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'
    products = ["Laptop", "Tablet", "Smartwatch", "Smartphone", "TV", "Monitor", "Camera"]
    manufacturers = ["Apple", "Samsung", "Sony", "Xiaomi", "Lonovo", "Dell", "LG", "Microsoft"]
    probability_True_False = [0.75, 0.25]
    data = {"Name": str,
            "Model": str,
            "Unit price in $": float,
            "Available": bool}


    def __init__(self) -> None:
        self.generate_product()
        

    def generate_product(self) -> None:
        product = random.choice(self.products)
        manufacturer = random.choice(self.manufacturers)

        self.data = {"Name": self.generate_name(product, manufacturer),
                    "Model": self.generate_model(product, manufacturer),
                    "Unit price in $": self.generate_unit_price(),
                    "Available": self.generate_status()}


    @validate_varchar
    def generate_name(self, product: str, manufacturer: str) -> str:
        return f"{product} {manufacturer}"


    @validate_varchar
    def generate_model(self, product: str, manufacturer: str) -> str:
        appendix = str().join(random.choice(self.characters) for i in range(10))
        return f"{manufacturer[0]}{product[0]}-"+appendix
    

    @validate_int_and_float
    def generate_unit_price(self) -> float:
        return np.round(np.random.uniform(400, 2000), 2)


    def generate_status(self) -> bool:
        return bool(np.random.choice([True,False], 1, p=self.probability_True_False)[0])


    def values(self) -> str:
        return str(self.data.values())[13:-2]


    def __getitem__(self, arg: str) -> Union[str, float, bool]:
        if arg in self.data.keys():
            return self.data[arg]
        else:
            raise ValueError(f"{__class__} does not contain key: '{arg}'. Available keys: {str(self.data.keys())[11:-1]}")


    def __repr__(self) -> str:
        return str(self.data)

