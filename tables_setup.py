"""
SQL tables setup for classes: user, product, order.
"""

import datetime
from typing import Union #Python 3.5 - 3.9

class tables_setup:
    max_float_len = 7
    max_str_len = 70


    @classmethod
    def get(self, class_) -> dict:
        data = dict(class_.__dict__)['data']
        setup = dict()
        for key in data.keys():
            setup[key] = self.select_variable(class_, key, data[key])
        return setup


    def select_variable(class_, key: str, var: Union[int, float, str, bool, datetime.date]) -> str:
        if var is int:
            return "INT"
        elif var is float:
            return f"FLOAT({str(tables_setup.max_float_len)})"
        elif var is str:
            return f"VARCHAR({str(tables_setup.max_str_len)})"
        elif var is bool:
            return "BOOL"
        elif var is datetime.date:
            return "DATE"
        else:
            raise ValueError(f"Class {class_.__name__} has unecpected type ({var.__name__}) of {key}. Should be int, float, str, bool or datetime.date.")
