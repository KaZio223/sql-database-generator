"""
MySQL database generating module. 
"""

from typing import Generator
from typing_extensions import Self
from data_generators.users import *
from data_generators.products import *
from data_generators.orders import *
from data_generators.ordered_products import *
from config import server_config
from tables_setup import *
import mysql.connector
import logging

LOGIN = server_config()

class MySQL_db_generator:
    def __init__(self, number_of_users: int, number_of_products: int, number_of_orders: int, database="MyShop") -> None:
        self._database_name = database
        self.number_of_users = number_of_users
        self.number_of_products = number_of_products
        self.number_of_orders = number_of_orders
        self.tables = {"Users": user,
                       "Products": product,
                       "Orders": order,
                       "OrderedProducts": ordered_products}


    def __enter__(self) -> Self:
        return self


    def __exit__(self, type, value, traceback):
        if type is None:
            logging.info("Program executed successfully.")
        else:
            logging.info(f"Traceback: type '{str(type)}', value '{str(value)}', traceback '{str(traceback)}'.")
        self.db.close()
        logging.warning("Connection closed.")


    def initiate_database(self) -> None:
        self.connect_to_server()
        self.connect_to_database()
        self.create_tables()


    def generate_data(self) -> None:
        self.generate_data_for_table("Users", self.number_of_users)
        self.generate_data_for_table("Products", self.number_of_products)
        self.generate_data_for_table("Orders", self.number_of_orders, self.number_of_users)
        self.generate_data_for_table("OrderedProducts", self.number_of_orders)


    def connect_to_server(self) -> None:
        try:
            self.db = mysql.connector.connect(**LOGIN.return_config())
            logging.info("Connected to database.")

            if not self.db.is_connected():
                error_message = f"{__class__} was unable to connect to MySQL server. Please check your configuration."
                logging.error(error_message)
                raise ConnectionError(error_message)
            else:
                logging.warning("Connection initiated.")
        except:
            error_message = f"{__class__} was unable to create mysql.connector object. Please check your configuration."
            logging.error(error_message)
            raise ConnectionError(error_message)


    def run_command(self, command: str) -> mysql.connector.cursor:
        cursor = self.db.cursor()
        cursor.execute(command)
        return cursor


    def cursor_first_args(self, cursor_result: list, first: int, last=None) -> Generator:
        if last is None:
            for i in cursor_result:
                yield i[first]
        else:
            for i in cursor_result:
                yield i[first:last]


    def connect_to_database(self) -> None:
        if self.database_exists():
            self.db.connect(database=self._database_name)
            self.drop_tables()
            logging.warning(f"Connected to {self._database_name}. All existing tables removed.")
        else:
            _ = self.run_command(f"CREATE DATABASE {self._database_name}")
            self.db.connect(database=self._database_name)
            logging.info(f"Database {self._database_name} successfully created.")


    def database_exists(self) -> bool:
        cursor = self.run_command("SHOW DATABASES")
        result = cursor.fetchall()
        return self._database_name in self.cursor_first_args(result, 0)


    def drop_tables(self) -> None:
        cursor = self.run_command(f"SHOW TABLES")
        result = cursor.fetchall()
        for table in self.cursor_first_args(result, 0):
            cursor = self.db.cursor()
            cursor.execute(f"DROP TABLE `{table}`")


    def create_tables(self) -> None:
        for key in self.tables.keys():
            setup = tables_setup.get(self.tables[key])
            self.run_command(f"CREATE TABLE {key} (id INT AUTO_INCREMENT PRIMARY KEY, {self.prepare_query_from_dict(setup)})")
            logging.info(f"Table {key} successfully created.")


    def prepare_query_from_dict(self, dict_: dict) -> str:
        rv = str()
        for key in dict_.keys():
            rv += f'`{key}` '
            rv += dict_[key]
            rv += ', '
        return rv[:-2]


    def generate_data_for_table(self, table_name: str, n_rows: int, *args) -> None:
        logging.info(f"Creating data for {table_name}.")
        columns = str(list(tables_setup.get(self.tables[table_name])))[1:-1].replace("'", "`")
        for i in range(n_rows):
            if table_name == "OrderedProducts":
                object_rv = self.tables[table_name](self.number_of_users, i+1)
                for j in range(len(object_rv)):
                    query = f"INSERT INTO {table_name} ({columns}) VALUES ({object_rv.values(j)})"
                    self.run_command(query)
            else:
                object_rv = self.tables[table_name](*args)
                query = f"INSERT INTO {table_name} ({columns}) VALUES ({object_rv.values()})"
                self.run_command(query)
        self.db.commit()


    def drop_database(self) -> None:
        self.run_command(f"DROP DATABASE {self._database_name}")

