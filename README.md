# SQL database generator
Python MySQL database generator. Version 2.0

## Description
The application is software that automatically connects to a MySQL database specified and encrypted in the "config.py" file. 
The configuration file is encrypted based on the RSA method and can be adapted to the specific needs of the user (an example of data encryption is shown in lines 59-70). 
The program automatically detects whether the necessary tables exist, contains the data, and creates them if necessary.

The database contains 4 tables: 
+ Users ("ID", "First name", "Last name", "Address", "City", "Post code", "State", "e-mail"); 
+ Products ("ID", "Name", "Model", "Unit price in $", "Available");
+ Orders ("ID", "User ID", "Date");
+ OrderedProducts ("ID", "Order ID", "Product ID", "Quantity").

The number of records entered in each table can be specified in the "main.py" file.

To run the application, there must be 1 additional "private_key.txt" file with the private RSA key corresponding to the public key in the "public_kay.txt" file.

To run the application (customize and) run/execute the python file "main.py".

## Installation
The application was written using Python 3.6 and MySQL 8.0.30 versions. 

Necessary python libraries: 
+ nympy
+ random
+ datetime
+ logging
+ rsa
+ typing
+ typing_extensions
+ mysql.connector
+ names
+ random_address
+ os
+ sys
+ unittest
+ inspect

## Tables visualisation
### Users
![Image showing the Users table example](screens/Users.png "Users table visualisation")
### Products
![Image showing the Products table example](screens/Products.png "Products table visualisation")
### Orders
![Image showing the Orders table example](screens/Orders.png "Orders table visualisation")
### OrderedProducts
![Image showing the OrderedProducts table example](screens/OrderedProducts.png "OrderedProducts table visualisation")

## Author
Karol Ziółkowski

## License
Open source project

## Project status
Fully executable. Next step will be application of SQL foreign keys, which permit cross-referencing across tables.
